## code to prepare `DATASET` dataset goes here

library(dplyr)
library(ggplot2)
install.packages("sinew")

my_dataset <- dplyr::slice_sample(diamonds, prop = 0.2)

#Ecrire la base dans répertoire data
usethis::use_data(my_dataset, overwrite = TRUE)

#Ecrire le squelette de la doc (metadonnees) de la base, dans le bon fichier doc
cat(sinew::makeOxygen("my_dataset"),
    file = "R/doc_my_dataset.R")
#Note : la doc des data doit se trouver dans le répertoire R/
rstudioapi::navigateToFile("R/doc_my_dataset.R")

#Ajout de la documentation R oxygen
attachment::att_amend_desc()

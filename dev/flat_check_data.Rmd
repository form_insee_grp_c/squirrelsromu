---
title: "flat_check_data.Rmd empty"
output: html_document
editor_options: 
  chunk_output_type: console
---

```{r development, include=FALSE}
library(testthat)
library(dplyr)
```

```{r development-load}
# Load already included functions if relevant
pkgload::load_all(export_all = FALSE)
```

# My function check_primary_color_is_ok()

```{r function-check_primary_color_is_ok}
#' check_data Title
#' 
#' @param string Un vecteur de chaine de caractere. Une ou plusieurs couleurs.
#' @export
#'
#' @examples
#' 
check_primary_color_is_ok <- function(string) {
  all_colors_OK <- all(
    string %in% c("Gray", "Cinnamon", "Black", NA)
  )
  if (isFALSE(all_colors_OK)) {
    stop("Erreur : au moins une couleur d'ecureuil n'est pas Grey, Black ou Cinnamon")
  }
  else{
    message("C'est bon, les couleurs d'ecureuil sont valides")
  }
  #
  return(all_colors_OK)
}
```

```{r examples-check_primary_color_is_ok}
liste_couleur <- c("Gray","Black")

check_primary_color_is_ok(liste_couleur[1])
check_primary_color_is_ok(liste_couleur)
```

```{r tests-check_primary_color_is_ok}
test_that("check_primary_color_is_ok", {
  expect_true(inherits(check_primary_color_is_ok, "function"))
  
  expect_message(
    object = check_primary_color_is_ok(string = "Cinnamon"), 
    regexp = "C'est bon, les couleurs d'ecureuil sont valides")
  
  expect_true(
    object = check_primary_color_is_ok(string = "Black"))
    
  expect_true(
    object = check_primary_color_is_ok(string = c("Gray","Black")))
  
  expect_error(
    object = check_primary_color_is_ok(string = "Blue"))
})
```

# My function check_squirrel_data_integrity()

```{r function-check_squirrel_data_integrity}
#' check_data Title
#' 
#' @param df_squirrels Un data-frame. Avec la colonne 'primary_fur_color'
#' @export
#'
#' @examples
#' 
check_squirrel_data_integrity <- function(df_squirrels) {
  is_present_primary_fur_color <- "primary_fur_color" %in% names(df_squirrels)
  if (isFALSE(is_present_primary_fur_color)) {
    stop("Erreur : La variable primary_fur_color est absente du jeu de donnees")
  }
  
  primary_colors_ok <- check_primary_color_is_ok(string = df_squirrels$primary_fur_color)
  
  if(isTRUE(primary_colors_ok)) {
    message("Toutes les couleurs des ecureuils sont valides")
  }
  
}
```

```{r examples-check_squirrel_data_integrity}
nyc_squirrels <- readr::read_csv(
system.file("nyc_squirrels_act_sample.csv", package = "squirrelsromu")
)

check_squirrel_data_integrity(df_squirrels = nyc_squirrels)
```

```{r tests-check_squirrel_data_integrity}
test_that("check_squirrel_data_integrity", {
  expect_true(inherits(check_squirrel_data_integrity, "function"))
  
nyc_squirrels <- readr::read_csv(
system.file("nyc_squirrels_act_sample.csv", package = "squirrelsromu")
)
  
  expect_message(
    object = check_squirrel_data_integrity(df_squirrels = nyc_squirrels), 
    regexp = "Toutes les couleurs des ecureuils sont valides")
  
  nyc_squirrels_wrong <- nyc_squirrels %>% 
    dplyr::select(-primary_fur_color)
  
  expect_error(
    object = check_squirrel_data_integrity(df_squirrels = nyc_squirrels_wrong))
    
 
})
```


```{r development-inflate, eval=FALSE}
# Run but keep eval=FALSE to avoid infinite loop
# Execute in the console directly
fusen::inflate(flat_file = "dev/flat_check_data.Rmd", 
               vignette_name = "Check Data",
               overwrite = TRUE)
```


# WARNING - Generated by {fusen} from /dev/flat_check_data.Rmd: do not edit by hand

test_that("check_primary_color_is_ok", {
  expect_true(inherits(check_primary_color_is_ok, "function"))
  
  expect_message(
    object = check_primary_color_is_ok(string = "Cinnamon"), 
    regexp = "C'est bon, les couleurs d'ecureuil sont valides")
  
  expect_true(
    object = check_primary_color_is_ok(string = "Black"))
    
  expect_true(
    object = check_primary_color_is_ok(string = c("Gray","Black")))
  
  expect_error(
    object = check_primary_color_is_ok(string = "Blue"))
})

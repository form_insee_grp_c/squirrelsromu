# squirrelsromu 0.0.0.9000

## Initial version

- 'get_message_fur_color' : pour faire apparaitre un message indiquant la couleur d'ecurueil choisie

- 'check_primary_fur_color' : verifier si un vecteur de couleur est bien compris dans les couleurs naturelles des ecureuils (en anglais) : Black, Grey ou Cinnamon
